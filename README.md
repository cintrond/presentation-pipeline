# RMD to Reveal.JS/PDF/PPTX Presentation Template

This is a template repository that renders R Markdown to HTML slides (Reveal.js), PDF with DeckTape, and PPTX with LibreOffice. 
All within the simplicity of Gitlab's CI interface. 

It depends on the following absolutely wonderful open-source projects: 

- [R Markdown](https://rmarkdown.rstudio.com/)
- [Pandoc](https://pandoc.org/)
- [Reveal.js](https://revealjs.com/)
- [DeckTape](https://github.com/astefanutti/decktape)
- [LibreOffice](https://www.libreoffice.org/)

## To Use

Clone/re-init or fork this repo and follow these basic instructions. 

There are only two existing files that you might need to modify. 
First is md/slides.Rmd - this is where you write your slides with R Markdown. 
Since this is not using the RStudio template for Reveal.js, you are really rendering R Markdown to Reveal.js by first creating an intermediate markdown file with ```rmarkdown::render()```  and then converting the markdown to Reveal.js slides with Pandoc. 
This makes it easier to upgrade to the newest version of Reveal.js. 

Second is bib/manual_references.bib. 
During the build process, pubmed ID-based references are extracted from the R Markdown file and used to create bib/references.bib. 
The manual_references file is added to this reference file, which is provided to Pandoc to produce in-slide citations. 
So you can provide non-PMID references bibtex to the manual_references.bib file. 

You could also modify the other files if you want to alter the formatting (e.g. css, csl).
Any images you want to include in the presentation go in the images folder. 
They will be accessible to the presentation via the images/file.png path in the built presentation. 

Gitlab will render Reveal.js. PDF, and PPTX every time you push to master. 
You can modify this behavior (which you should) by either making a working branch, or setting the .gitlab-ci.yml to only render on release or something like that. 

## Building slides locally

All files are generated automatically when you pusht to the master branch. 
However, you probably want to view slides lically when you're working on them. 

First, make sure you install R, R Markdown, pandoc and pandoc-citeproc locally.
Or easier - install Docker and run:

```bash
docker run -it --rm -v $PWD:/slides adamslab/doc_builder:latest bash
```

and do the following inside the /slides directory. 

You can then build the HTML slides locally by running ```make build```.
This will create a new 'public' directory. 
You can view your slides by opening public/index.html in your browser. 

### PDF and PPTX Locally

To make a PDF of the slides, run: 

```bash
/node_modules/.bin/decktape --chrome-arg=--no-sandbox -s '1512x1137' public/index.html public/slides.pdf
```
from within the Docker container. 

And finally, to make a PPTX file from the PDF, run: 

```bash
soffice --infilter="impress_pdf_import" --convert-to pptx:"Impress MS PowerPoint 2007 XML" public/slides.pdf --outdir public/
```

## Contributing

I made this for my own convenience, but thought others might benefit. 
If you find an issue, please feel free to report or submit a pull-request. 
